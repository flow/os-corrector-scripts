#include <stdio.h>
#include "stubs2.h"
#include "fat.h"

pte_t my_pte = 0;

pte_t* walk(pagetable_t pt, uint64 va, int alloc){
  return &my_pte;
}

void print_vma_result(int err){
  if (err == ENOVMA) printf("ENOVMA");
  else if (err == ENOMEM) printf("ENOMEM");
  else if (err == ENOFILE) printf("ENOFILE");
  else if (err == EBADPERM) printf("EBADPERM");
  else if (err == EMAPFAILED) printf("EMAPFAILED");
  else printf("%d", err);
}

int main(){

  install_segv_handler();

  pagetable_t pt;
  int res;

  struct proc p;
  memset(&p, 0, sizeof(p));
  set_myproc(&p);

  add_memory_area(&p, 0x1000, 0x2000);

  res = do_allocate(pt, myproc(), 0);
  printf("do_allocate returned "); print_vma_result(res); printf("\n");
  assert(res == ENOVMA, "do_allocate should return ENOVMA on address outside a VMA (pte=0)", NOSTOP);

  my_pte |= PTE_V;
  res = do_allocate(pt, myproc(), 0);
  printf("do_allocate returned "); print_vma_result(res); printf("\n");
  assert(res == ENOVMA, "do_allocate should return ENOVMA on address outside a VMA (pte!=0, V=1, U=0)", NOSTOP);

  my_pte |= PTE_V | PTE_U;
  res = do_allocate(pt, myproc(), 0);
  printf("do_allocate returned "); print_vma_result(res); printf("\n");
  assert(res == ENOVMA, "do_allocate should return ENOVMA on address outside a VMA (pte!=0, V=1, U=1)", NOSTOP);

  res = do_allocate(pt, myproc(), 0x1000);
  printf("do_allocate returned "); print_vma_result(res); printf("\n");
  assert(res == 0, "do_allocate should return 0 on address at start of a VMA", NOSTOP);

  res = do_allocate(pt, myproc(), 0x2000);
  printf("do_allocate returned "); print_vma_result(res); printf("\n");
  assert(res == ENOVMA, "do_allocate should return ENOVMA on address at end of a VMA", NOSTOP);


  my_pte = 0;
  res = do_allocate(pt, myproc(), 0x1000);
  printf("do_allocate returned "); print_vma_result(res); printf("\n");
  /* printf("ENOVMA = %lx\n", ENOVMA); */
  /* printf("ENOMEM = %lx\n", ENOMEM); */
  /* printf("ENOFILE = %lx\n", ENOFILE); */
  /* printf("EBADPERM = %lx\n", EBADPERM); */
  /* printf("EMAPFAILED = %lx\n", EMAPFAILED); */
  assert(res == 0, "do_allocate should return 0 on in-VMA address", NOSTOP);
  assert(my_pte != 0, "do_allocate should update pte to non-null value", NOSTOP);
  assert(my_pte | PTE_V, "do_allocate should update pte bit V", NOSTOP);
  assert(my_pte | PTE_W, "do_allocate should update pte bit W", NOSTOP);
  assert(my_pte | PTE_R, "do_allocate should update pte bit R", NOSTOP);
  assert(my_pte | PTE_X, "do_allocate should update pte bit X", NOSTOP);
  assert(my_pte | PTE_U, "do_allocate should update pte bit U", NOSTOP);

  // TODO: test mappages returns EMAPFAILED

  return 0;
}
