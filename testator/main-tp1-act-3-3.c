#include <stdio.h>
#include "stubs1.h"
#include "fat.h"

uint64 locks_held(){
  uint64 locks_held = 0;
  for(int i = 0; i < NPROC; i++){
    int b = proc[i].lock.num != 0;
    locks_held |= b ? (1L << i) : 0;
  }
  return locks_held;
}

char* print_locks_held(uint64 l){
  char *s = malloc(64 * 3 * sizeof(char));
  char* p = s;
  for(int i = 0; i < NPROC; i++){
    int b = ((l & (1L << i))) != 0;
    if(b){
      int n = snprintf(p, 4, "%d ", i);
      p+=n;
    }
  }
  *p = 0;
  return s;
}

int main(){

  initator();

  for(int i = 0; i < NPROC; i++){
    proc[i].lock.num=0;
    proc[i].state = UNUSED;
  }

  for(int i = 3; i < 6; i++){
    proc[i].state = RUNNABLE;
    proc[i].priority = 5;
    proc[i].pid = i;
    acquire(&prio_lock);
    acquire(&proc[i].lock);
    insert_into_prio_queue(&proc[i]);
    release(&proc[i].lock);
    release(&prio_lock);
  }

  // nice itself does not have to check validity of priority: this is done in sys_nice...
  printf("processes 3, 4, 5 are runnable with priority 5, others are UNUSED\n");
  assert(nice(3,4) == 1, "nice(3,4) should return 1", NOSTOP);
  assert(proc[3].priority == 4, "nice(3,4) should have changed priority", NOSTOP);
  char * s = print_locks_held(locks_held());
  printf("locks held: %s\n", s);
  free(s);
  assert(locks_held() == 0, "should not hold locks", NOSTOP);
  assert(nice(2,8) == 0, "nice(2,8) should return 0", NOSTOP);
  s = print_locks_held(locks_held());
  printf("locks held: %s\n", s);
  free(s);
  assert(locks_held() == 0, "should not hold locks", NOSTOP);

  return 0;
}
