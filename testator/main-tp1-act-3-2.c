#include <stdio.h>
#include "stubs1.h"
#include "fat.h"

/* int nice(int pid, int priority){ */
/*   return 0; */
/* } */

void test_sys_nice(int pid, int priority, int expectret){
  setup_syscall_args(2, pid, priority);
  int ret = sys_nice();
  char* str = easy_snprintf("sys_nice(pid=%d, priority=%d) should return %d (got %d)", pid, priority, expectret, ret);
  assert(expectret == ret, str, NOSTOP);
  free(str);
}

int main(){

  initator();

  test_sys_nice(1, 1, 0);
  test_sys_nice(1, -1, -1);
  test_sys_nice(1, 9, 0);
  test_sys_nice(1, 15, -1);

  return 0;
}
