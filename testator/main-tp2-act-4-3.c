#include <stdio.h>
#include "stubs2.h"
#include "fat.h"

void show_vmas(){
  struct vma* ma = myproc()->memory_areas;
  while(ma){
    printf("  %lx - %lx\n", ma->va_begin, ma->va_end);
    ma = ma -> next;
  }
}


int main(){

  install_segv_handler();

  struct proc p;
  memset(&p, 0, sizeof p);
  p.heap_vma = add_memory_area(&p, 0x5000, 0x5000);
  struct vma* ma = p.heap_vma;
  p.sz = 0x5000;
  set_myproc(&p);

  int ret = growproc(0x1000);

  assert(ret == 0, "growproc(0x1000) should return 0", NOSTOP);
  assert(p.heap_vma == ma, "growproc(0x1000) should not haved changed location of VMA", NOSTOP);
  assert(p.heap_vma->va_begin == 0x5000, "growproc(0x1000) should not haved changed va_begin", NOSTOP);
  assert(p.heap_vma->va_end == 0x6000, "growproc(0x1000) should haved changed va_end to 0x6000", NOSTOP);
  assert(p.sz == p.heap_vma->va_end, "growproc should set sz == heap_vma->va_end", NOSTOP);

  ret = growproc(-0x1000);

  assert(ret == 0, "growproc(-0x1000) should return 0", NOSTOP);
  assert(p.heap_vma == ma, "growproc(-0x1000) should not haved changed location of VMA", NOSTOP);
  assert(p.heap_vma->va_begin == 0x5000, "growproc(-0x1000) should not haved changed va_begin", NOSTOP);
  assert(p.heap_vma->va_end == 0x5000, "growproc(-0x1000) should haved changed va_end to 0x5000", NOSTOP);
  assert(p.sz == p.heap_vma->va_end, "growproc should set sz == heap_vma->va_end", NOSTOP);

  ret = growproc(-0x1000);
  assert(ret == -1, "growproc(-0x1000) should return -1", NOSTOP);
  assert(p.heap_vma == ma, "growproc(-0x1000) should not haved changed location of VMA", NOSTOP);
  assert(p.heap_vma->va_begin == 0x5000, "growproc(-0x1000) should not have changed va_begin", NOSTOP);
  assert(p.heap_vma->va_end == 0x5000, "growproc(-0x1000) should not have changed va_end", NOSTOP);
  assert(p.sz == p.heap_vma->va_end, "growproc should set sz == heap_vma->va_end", NOSTOP);

  ret = growproc((1L << 23));
  assert(ret == -1, "growproc(1L << 23) should return -1", NOSTOP);
  assert(p.heap_vma == ma, "growproc(1L << 23) should not haved changed location of VMA", NOSTOP);
  assert(p.heap_vma->va_begin == 0x5000, "growproc(1L << 23) should not have changed va_begin", NOSTOP);
  assert(p.heap_vma->va_end == 0x5000, "growproc(1L << 23) should not have changed va_end", NOSTOP);
  assert(p.sz == p.heap_vma->va_end, "growproc should set sz == heap_vma->va_end", NOSTOP);



  return 0;
}
