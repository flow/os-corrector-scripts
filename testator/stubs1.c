#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <sys/mman.h>
#include "stubs1.h"
#include <execinfo.h>
#define __USE_GNU
#define _GNU_SOURCE
#include <dlfcn.h>

void (*original_exit)(int) = NULL;
void (*original_sleep)(int) = NULL;
jmp_buf segv_jmp_buf;
int stub_file_num = 0;
struct file stub_file;
struct proc *_myproc = NULL;
struct file * stub_files[20];
struct proc* initproc = NULL;

/* #define UNW_LOCAL_ONLY */
/* #include <libunwind.h> */
/* TODO: addr2line (fork+exec+pipe) + libunwind dans Dockerfile... */
/* void show_backtrace (void) { */
/*   unw_cursor_t cursor; */
/*   unw_context_t uc; */
/*   unw_word_t ip, offset; */

/*   unw_getcontext(&uc); */
/*   unw_init_local(&cursor, &uc); */
/*   while (unw_step(&cursor) > 0) { */
/*     unw_get_reg(&cursor, UNW_REG_IP, &ip); */
/*     /\* unw_get_reg(&cursor, UNW_REG_SP, &sp); *\/ */
/*     /\* printf ("ip = %lx, sp = %lx\n", (long) ip, (long) sp); *\/ */
/*     char sym[256]; */
/*     if (unw_get_proc_name(&cursor, sym, sizeof(sym), &offset) == 0) { */
/*       printf("Called from (%s+0x%lx)\n", sym, offset); */
/*     } else { */
/*       printf("Called from ??\n"); */
/*     } */
/*   } */
/* } */

void
print_trace (void)
{
  void *array[10];
  char **strings;
  int size, i;

  size = backtrace (array, 10);
  strings = backtrace_symbols (array, size);
  if (strings != NULL)
    {

      /* printf ("Obtained %d stack frames.\n", size); */
      for (i = 0; i < size; i++){
        printf ("%s\n", strings[i]);
        char* str = strdup(strings[i]);
        char* fname = str;
        char* fname_end = str;
        while(*fname_end && (*fname_end != '('))
          fname_end++;
        if (*fname_end == '(') *fname_end = 0;
        char* funname = fname_end + 1;
        char* funname_end = funname;
        while(*funname_end != ')')
          funname_end++;
        *funname_end = 0;
        char* addr = funname_end + 1;
        while(*addr && (*addr != '['))
          addr++;
        addr++;
        char* addr_end = addr + 1;
        while (*addr_end && *addr_end != ']')
          addr_end++;
        if (*addr_end == ']')
          *addr_end = 0;
        char* cmd = easy_snprintf("addr2line -e %s -a %s", fname, addr);
        free(str);
        FILE *fp = popen(cmd, "r");
        if(!fp){
          printf("Failed to run %s.\n", cmd);
          free(cmd);
          continue;
        }
        char line[100];
        fgets(line, sizeof(line), fp); // skip first line
        fgets(line, sizeof(line), fp);
        for(int i = 0; i < sizeof(line); i++){
          if(line[i] == '\n'){
            line[i] = 0;
            break;
          }
        }
        printf("%s: %s\n", line, funname);
        free(cmd);
        pclose(fp);


      }

    }

  free (strings);
  /* show_backtrace(); */
}

void assert(int condition, char* descr, int stop){
  if(!condition){
    printf("KO %s\n", descr);
    if(stop) original_exit(EXIT_FAILURE);
  }
  else {
    printf("OK %s\n", descr);
  }
}


char* easy_snprintf(char* fmt, ...){
  va_list args;
  va_start(args, fmt);
  int n = vsnprintf(NULL, 0, fmt, args);
  va_end(args);
  va_start(args, fmt);
  char * s = malloc(n+1);
  vsnprintf(s, n+1, fmt, args);
  va_end(args);
  return s;
}



void handler(int sig, siginfo_t *si, void *unused)
{
  printf("Got SIGSEGV at address: 0x%lx\n",(long) si->si_addr);
  print_trace();
  assert(0, "Got segmentation fault.", NOSTOP);
  longjmp(segv_jmp_buf, 1);
  original_exit(EXIT_FAILURE);
}

void install_segv_handler(){
  struct sigaction sa;

  sa.sa_flags = SA_SIGINFO;
  sigemptyset(&sa.sa_mask);
  sa.sa_sigaction = handler;
  if (sigaction(SIGSEGV, &sa, NULL) == -1){
    perror("sigaction");
    original_exit(EXIT_FAILURE);
  }
  if(setjmp(segv_jmp_buf)){
    // setjmp can be called from elsewhere, below is just the default handler (see e.g main-tp2-act-4-5.c)
    original_exit(EXIT_FAILURE);
  }
}

void initator(){
  // https://stackoverflow.com/questions/54574133/python-capture-stdout-of-crashed-program-via-subprocess
  setvbuf(stdout, NULL, _IONBF, 0);
  // https://stackoverflow.com/a/68394074/2357463
  original_exit = dlsym(RTLD_NEXT, "exit");
  original_sleep = dlsym(RTLD_NEXT, "sleep");
  install_segv_handler();
}

static uint64 syscall_args[6];
void setup_syscall_args(int count, ...){
  va_list args;
  va_start(args, count);
  if (count > 6) count = 6;
  for(int i = 0; i < count; i++){
    syscall_args[i] = va_arg(args, uint64);
  }
  va_end(args);
}

uint64 argraw(int n){
  if (0 <= n && n < 6) return syscall_args[n];
  return -1;
}

int argint(int n, int *ip){
  *ip = argraw(n);
  return 0;
}

int argaddr(int n, uint64 *ip){
  *ip = argraw(n);
  return 0;
}

int
fetchaddr(uint64 addr, uint64* ip)
{
  uint64* p = (uint64*)addr;
  *ip = *p;
  return 0;
}


int
fetchstr(uint64 addr, char *buf, int max)
{
  strncpy(buf, (char*)addr, max);
  return strlen(buf);
}

int
argstr(int n, char *buf, int max)
{
  uint64 addr;
  if(argaddr(n, &addr) < 0)
    return -1;
  return fetchstr(addr, buf, max);
}


static uint64 current_date = 0;
void acquire(struct spinlock* l){
  if(l->num) {
    panic("acquire already held lock");
    return;
  }
  l->num++;
  l->date = current_date;
  current_date++;
}

void release(struct spinlock* l){
  if(!l->num) {
    panic("release not holding");
    return;
  }

  l->num--;
}

int holding(struct spinlock *lk)
{
  return lk->num != 0;
}

struct proc proc[NPROC];
struct list_proc* prio[NPRIO];
struct spinlock prio_lock;
int called;

// Needs lock on p and prio_lock[p->priority]
void insert_into_prio_queue(struct proc* p){
  called = 1;
  if (!holding(&p->lock)){
    assert(0, "insert_into_prio_queue: should hold p->lock", 1);
  }
  if (!holding(&prio_lock)){
    assert(0, "insert_into_prio_queue: should hold prio_lock", 1);
  }

  struct list_proc* new = bd_malloc(sizeof(struct list_proc));
  new->next = 0;
  new->p = p;
  if(!prio[p->priority]){
    prio[p->priority] = new;
  }
  else {
    struct list_proc* last = prio[p->priority];
    while(last && last->next){
      last = last->next;
    }
    last->next = new;
  }
}

// Needs lock on p and prio_lock[p->priority]
void remove_from_prio_queue(struct proc* p){
  called = 1;
  if (!holding(&p->lock)){
    assert(0, "remove_from_prio_queue: should hold p->lock", 1);
  }
  if (!holding(&prio_lock)){
    assert(0, "remove_from_prio_queue: should hold prio_lock", 1);
  }
  struct list_proc* old = prio[p->priority];
  struct list_proc* prev = 0;
  struct list_proc* head = old;

  while(old){
    if(old->p == p) {
      if(old == head){
        head = old->next;
      } else {
        prev->next = old->next;
      }
      bd_free(old);
      break;
    }
    prev = old;
    old = old->next;
  }

  prio[p->priority] = head;
}

static int filealloc_succeed = 0;
void set_filealloc_succeed(int x){
  filealloc_succeed = x;
}

static int fdalloc_retval = 0;
void set_fdalloc_retval(int x){
  fdalloc_retval = x;
}

struct file*
filealloc(void)
{
  if(filealloc_succeed){
    stub_file_num++;
    return &stub_file;
  } else {
    return 0;
  }
}

struct file * stub_files[20];

int argfd(int n, int *pfd, struct file **pf){
  int fd;
  struct file * f;
  if(argint(n, &fd) < 0) return -1;
  if(fd < 0 || fd >= 20 || (f=stub_files[fd]) == 0)
    return -1;
  if(pfd) *pfd = fd;
  if(pf) *pf = f;
  return 0;
}

int fdalloc(struct file *f){
  if(fdalloc_retval >= 0 && fdalloc_retval < 20){
    stub_files[fdalloc_retval] = f;
  }
  return fdalloc_retval;
}

void fileclose(struct file* f){
  assert(f == &stub_file, "fileclose", NOSTOP);
  stub_file_num--;
}

void
initsleeplock(struct sleeplock *lk, char *name){
  lk->init = 1;
}

void set_myproc(struct proc* p){ _myproc = p; }

struct proc* myproc(){
  return _myproc;
}

struct cpu _mycpu;
struct cpu*     mycpu(void){
  return &_mycpu;
}

void
acquiresleep(struct sleeplock *lk)
{
  acquire(&lk->lk);
  lk->locked = 1;
  lk->pid = myproc()->pid;
  release(&lk->lk);
}

void
releasesleep(struct sleeplock *lk)
{
  acquire(&lk->lk);
  lk->locked = 0;
  lk->pid = 0;
  release(&lk->lk);
}


int
holdingsleep(struct sleeplock *lk)
{
  int r;
  
  acquire(&lk->lk);
  r = lk->locked && (lk->pid == myproc()->pid);
  release(&lk->lk);
  return r;
}



void
uvminit(pagetable_t pagetable, uchar *src, uint sz){
  
}


void panic(char* str){
  char* s = easy_snprintf("PANIC: %s\n", str);
  assert(0, s, NOSTOP);
  free(s);
  print_trace();
}

int
uvmcopy(pagetable_t old, pagetable_t new, uint64 sz){
  return 0;
}

/* void freeproc(struct proc *p){ */
  
/* } */
struct file*    filedup(struct file* f){
  return f;
}
struct inode*   idup(struct inode* i){
  return i;
}

void * bd_malloc(uint64 nbytes){
  return malloc(nbytes);
}
void bd_free(void *p){
  free(p);
}

char* safestrcpy(char *s, const char *t, int n){
  char* res = strncpy(s,t,n);
  res[n-1] = 0;
  return res;
}

void end_op(int x){}
void begin_op(int x){}
void            iunlockput(struct inode* ip){
}
void            iput(struct inode* ip){
}
void            ilock(struct inode* ip){
}
void            iunlock(struct inode* ip){
}
void            proc_freepagetable(pagetable_t pt, uint64 sz){
  free(pt);
}

char* strjoin(char **s){
  int n = 0;
  char** os = s;
  while(*s){
    n += strlen(*s) + 1;
    s++;
  }
  char* d = bd_malloc(n);
  s = os;
  char* od = d;
  while(*s){
    n = strlen(*s);
    safestrcpy(d, *s, n+1);
    d+=n;
    *d++ = ' ';
    s++;
  }
  d[-1] = 0;
  return od;
}


void
uvmclear(pagetable_t pagetable, uint64 va){
  
}

uint64
uvmalloc(pagetable_t pagetable, uint64 oldsz, uint64 newsz){
  if(newsz < oldsz) return oldsz;
  return newsz;
}

uint64
uvmdealloc(pagetable_t pagetable, uint64 oldsz, uint64 newsz){
  if(newsz >= oldsz)
    return oldsz;
  return newsz;
}

struct inode*   namei(char* path){
  FILE* fd = fopen(path, "r");
  return (struct inode*)fd;
}

void print_memory_area(struct proc *p, struct vma *ma) {
  printf("VA = [%lx; %lx[ RWX=%d%d%d", ma->va_begin, ma->va_end,
         (ma->vma_flags & VMA_R) != 0, (ma->vma_flags & VMA_W) != 0,
         (ma->vma_flags & VMA_X) != 0);
  if (ma->file) {
    printf(" file=%s off=0x%lx n=0x%lx", ma->file, ma->file_offset,
           ma->file_nbytes);
  }
  if (ma == p->stack_vma)
    printf(" [stack]");
  if (ma == p->heap_vma)
    printf(" [heap]");
  printf("\n");
}

/* Affiche la liste de VMAs associée à un processus. */
void print_memory_areas(struct proc *p) {
  struct vma *ma = p->memory_areas;
  printf("Memory areas:\n");
  while (ma) {
    print_memory_area(p, ma);
    ma = ma->next;
  }
  printf("==============\n");
}


uint64 walkaddr(pagetable_t pagetable, uint64 va){
  char* pa = get_memory_area_mem(myproc(), va);
  if(pa == 0){
    print_memory_areas(myproc());
    char* str = easy_snprintf("walkaddr: try to read/write outside a VMA (addr=0x%x)", va);
    panic(str);
    free(str);
  }
  return (uint64)pa;
}

int
copyout(pagetable_t pagetable, uint64 dstva, char *src, uint64 len){
  memcpy((char*)dstva, src, len);
  return 0;
}

int readi(struct inode* ip, int user_dst, uint64 dst, uint off, uint n){
  FILE* f = (FILE*)ip;
  char* ddst = user_dst ? (char*)walkaddr(myproc()->pagetable, dst) : (char*)dst;
  fseek(f, off, SEEK_SET);
  return fread(ddst, 1, n, f);
}

int
loadseg(pagetable_t pagetable, uint64 va, struct inode *ip, uint offset, uint sz)
{
  uint i, n;
  char* pa;

  /* if((va % PGSIZE) != 0) */
  /*   panic("loadseg: va must be page aligned"); */
  for(i = 0; i < sz; i += PGSIZE){
    pa = (char*)walkaddr(pagetable, va + i);
    if(pa == 0){
      panic("loadseg: address should exist");
      return -1;
    }
    if(sz - i < PGSIZE)
      n = sz - i;
    else
      n = PGSIZE;
    if(readi(ip, 0, (uint64)pa, offset+i, n) != n)
      return -1;
  }
  
  return 0;
}
pagetable_t proc_pagetable(struct proc *p){
  p->pagetable = malloc(4096);
  return p->pagetable;
}

struct vma *add_memory_area(struct proc *p, uint64 va_begin, uint64 va_end) {
  acquire(&p->vma_lock);
  struct vma *entry = bd_malloc(sizeof(struct vma));
  if (!entry) {
    panic("bd_malloc failed\n");
  }
  entry->va_begin = va_begin;
  entry->va_end = va_end;
  entry->file = 0;
  entry->vma_flags = 0;
  entry->file_offset = 0;
  entry->file_nbytes = 0;

  struct vma *ma = p->memory_areas;
  struct vma *pred = 0;
  while (ma) {
    if ((ma->va_begin == va_begin && ma->va_end > va_end) ||
        ma->va_begin > va_begin) {
      if (va_end > ma->va_begin)
        panic("Overlap in add_memory_area");
      entry->next = ma;
      if (pred)
        pred->next = entry;
      else
        p->memory_areas = entry;
      release(&p->vma_lock);
      return entry;
    } else {
      pred = ma;
      ma = ma->next;
    }
  }
  entry->next = 0;
  /* Mécanismes de hook ? */
  entry->mem = mmap(NULL, va_end - va_begin, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (pred)
    pred->next = entry;
  else
    p->memory_areas = entry;
  release(&p->vma_lock);
  return entry;
}

void free_vma(struct vma *vmas) {
  struct vma *ma = vmas;
  while (ma) {
    struct vma *old = ma;
    munmap((void*)ma->va_begin, ma->va_end - ma->va_begin);
    ma = ma->next;
    memset(old, 0, sizeof(struct vma));
    bd_free(old);
  }
}

struct vma *get_memory_area(struct proc *p, uint64 va) {
  struct vma *ma = p->memory_areas;
  while (ma) {
    if (ma->va_begin <= va && va < ma->va_end) {
      return ma;
    }
    ma = ma->next;
  }
  return 0;
}

char *get_memory_area_mem(struct proc *p, uint64 va) {
  struct vma *ma = p->memory_areas;
  while (ma) {
    if (ma->va_begin <= va && va < ma->va_end) {
      return ma->mem + (va - ma->va_begin);
    }
    ma = ma->next;
  }
  return 0;
}
// TODO: parameterize so that kalloc fails
void* kalloc(){ return malloc(4096);}
void kfree(void* p) { free(p); }

void sleep(void* chan, struct spinlock* lk){
  
}

jmp_buf sched_jmp_buf;
void sched(void){
  longjmp(sched_jmp_buf,1);
}


jmp_buf swtch_jmp_buf;
void swtch(struct context* from, struct context* to){
  swapcontext(&from->jb, &to->jb);
}


uchar initcode[50];

int nextpid = 1;
int allocpid(){
  int pid = nextpid;
  nextpid++;
  return pid;
}

void* forkret = NULL;

char* proc_state_to_string(int state){
  static char *states[] = {
    [UNUSED] =   "unused",
    [SLEEPING]=  "sleep ",
    [RUNNABLE] = "runble",
    [RUNNING]   ="run   ",
    [ZOMBIE]    ="zombie"
  };
 
  switch (state){
  case RUNNING:
  case RUNNABLE:
  case ZOMBIE:
  case SLEEPING:
  case UNUSED:
    return states[state];
  default:
    return "unknown";
  }
}
