#include <stdio.h>
#include "stubs2.h"
#include "fat.h"

int main(){

  install_segv_handler();

  int i;
  struct proc* p;
  userinit();
  p = initproc;
  set_myproc(initproc);
  initproc->state = RUNNING;
  char *argv[] = {"_ls", "arg1", 0};
  int ret = exec("_ls", argv);
  char *str = easy_snprintf("exec(\"_ls\", {\"_ls\", \"arg1\", 0}) should return 2 (got %d)", ret);
  assert(ret == 2, str, NOSTOP);
  free(str);
  assert(myproc()->stack_vma != 0, "exec should have set stack_vma", NOSTOP);
  assert(myproc()->heap_vma != 0, "exec should have set heap_vma", NOSTOP);
  assert(myproc()->heap_vma && myproc()->heap_vma->va_begin == myproc()->heap_vma->va_end, "exec should have set heap_vma to an empty VMA", NOSTOP);

  str = easy_snprintf("stack vma should begin at USTACK_BOTTOM (got %x)", myproc()->stack_vma->va_begin);
  assert(myproc()->stack_vma->va_begin == 256*1024*1024, str, NOSTOP);
  free(str);

  str = easy_snprintf("stack vma should end at USTACK_TOP (got %x)", myproc()->stack_vma->va_end);
  assert(myproc()->stack_vma->va_end == (256*1024 + 4)*1024, str, NOSTOP);
  free(str);

  return 0;
}
