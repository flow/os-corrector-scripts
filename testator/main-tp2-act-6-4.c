#include <stdio.h>
#include "stubs2.h"
#include "fat.h"

uint64 scause_expected = CAUSE_R;

int do_allocate(pagetable_t pagetable, struct proc* p, uint64 addr, uint64 scause){
  char *str = easy_snprintf("do_allocate expected to be called with cause 0x%x (got 0x%x)", scause_expected, scause);
  assert(scause == scause_expected, str, NOSTOP);
  free(str);
  return -1;
}


int do_allocate_range(pagetable_t pagetable, struct proc* p, uint64 addr, uint64 len, uint64 scause){
  char *str = easy_snprintf("do_allocate_range expected to be called with cause 0x%x (got 0x%x)", scause_expected, scause);
  assert(scause == scause_expected, str, NOSTOP);
  free(str);
  return -1;
}


int main(){

  install_segv_handler();

  pagetable_t pt;

  struct proc p;
  memset(&p, 0, sizeof p);
  set_myproc(&p);

  scause_expected = CAUSE_W;
  copyout(pt, 0, 0, 0);

  scause_expected = CAUSE_R;
  copyin(pt, 0, 0, 0);
  copyinstr(pt, 0, 0, 1);

}
