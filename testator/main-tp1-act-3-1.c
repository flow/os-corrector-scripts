#include <stdio.h>
#include "stubs1.h"
#include "fat.h"


void test_nice(int pid, int priority, int expectret){
  int ret = nice(pid, priority);
  char* str = easy_snprintf("nice(pid=%d, priority=%d) should return %d (got %d)", pid, priority, expectret, ret);
  assert(expectret == ret, str, NOSTOP);
  free(str);
}

int main(){

  initator();
  test_nice(1,4,0);
  test_nice(-1,4,0);
  test_nice(1,10,0);
  test_nice(18,4,0);
  test_nice(18,-1,0);
  test_nice(18,11,0);

  return 0;
}
