#include <stdio.h>
#include "stubs1.h"
#include "fat.h"

void f(void);

/* Preparing the context of a process : get the current context, specify a new
   stack and make context point to function f. */
void reset_proc_handler(struct proc* p){

  getcontext(&p->context.jb);
  p->context.jb.uc_stack.ss_sp = p->context.stk;
  p->context.jb.uc_stack.ss_size = sizeof(p->context.stk);
  p->context.jb.uc_link = NULL;
  makecontext(&p->context.jb, (void (*) (void))f, 0);
}

int testno = 0;

ucontext_t main_ctx;
char mstk[8192];

/* Fake process code :

   - check mycpu() and myproc() are not NULL
   - on iterations 0 and 2, test pid == 3
   - on iterations 1 and 3, test pid == 5
   - iteration 3, switch to "main_ctx", exiting the program

   - if not over, make process runnable again and switch bak to scheduler

 */
void f(){
  while(1){
    printf("New process elected by scheduler, round %d\n", testno);
    struct proc* p = NULL;
    assert(mycpu() != NULL, "mycpu() should not be NULL", STOP);
    p = mycpu()->proc;
    assert(p != NULL, "myproc() should not be NULL", STOP);
    assert(prio_lock.num == 0, "should not hold prio_lock while in process", NOSTOP);
    assert(p->lock.num != 0, "should hold process lock while in process", NOSTOP);
    assert(p->state == RUNNING, "process state should be RUNNING", NOSTOP);
    switch(testno){
    case 0:
    case 2:
      assert(p->pid == 3, "pid should be 3", NOSTOP);
      break;
    case 1:
    case 3:
      assert(p->pid == 5, "pid should be 5", NOSTOP);
      if (testno == 3) setcontext(&main_ctx);
      break;
    }
    testno++;
    // yielding
    // make process runnable again
    mycpu()->proc->state = RUNNABLE;
    // switching back to scheduler
    swtch(&mycpu()->proc->context, &mycpu()->scheduler);
  }
}

void do_test(){
  int i;
  struct proc* p;

  // Preparing process table
  // All process have priority 5, except processes 3 and 5 who have (higher) priority 3
  printf("All process have priority 5, except processes 3 and 5 who have (higher) priority 3\n");
  for(i = 0; i < NPROC; i++){
    proc[i].state = RUNNABLE;
    proc[i].priority = i == 3 || i == 5 ? 3 : 5;
    proc[i].pid = i;
    acquire(&prio_lock);
    acquire(&proc[i].lock);
    insert_into_prio_queue(&proc[i]);
    release(&proc[i].lock);
    release(&prio_lock);
  }

  // Initialization of every process's context
  for(i = 0; i < NPROC; i++){
    reset_proc_handler(&proc[i]);
  }

  // Preparing exit code.

  getcontext(&main_ctx);
  main_ctx.uc_stack.ss_sp = mstk;
  main_ctx.uc_stack.ss_size = sizeof mstk;
  main_ctx.uc_link = NULL;

  makecontext(&main_ctx, (void (*)(void))original_exit, 1, 0);

  printf("Launching scheduler\n");
  scheduler();
  printf("Not here!\n");

}

void reinit_prio_queues(){
  for(int i = 0; i < NPRIO; i++){
    prio[i] = NULL;
  }
}

int main(){

  initator();

  reinit_prio_queues();

  do_test();


  return 0;
}
