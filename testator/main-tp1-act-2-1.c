#include <stdio.h>
#include "stubs1.h"
#include "fat.h"

void test_no_process(){
  int i;
  struct proc* p;
  for(i = 0; i < NPRIO; i++){
    prio[i] = NULL;
  }
  printf("### All priority queues are empty.\n");
  printf("Calling pick_highest_priority_runnable_proc\n");
  p = pick_highest_priority_runnable_proc();
  if(!p)
    printf("returned NULL\n");
  else{
    printf("returned process %ld\n",p-proc);
  }
  assert(p == NULL, "#1 - No process: should return NULL", STOP);
  assert(prio_lock.num == 0, "#1 - No process: should not hold prio_lock", STOP);
}

void test_no_runnable_process(){
  int i;
  struct proc* p;
  int bug = 0;
  for(i = 0; i < NPROC; i++){
    proc[i].state = ZOMBIE;
    proc[i].priority = 3;
    acquire(&prio_lock);
    acquire(&proc[i].lock);
    insert_into_prio_queue(&proc[i]);
    release(&proc[i].lock);
    release(&prio_lock);
  }
  printf("### All priority queues are empty, except for priority 3, which contains all processes, all in the ZOMBIE state.\n");
  printf("Calling pick_highest_priority_runnable_proc\n");
  p = pick_highest_priority_runnable_proc();
  if(!p)
    printf("returned NULL\n");
  else{
    printf("returned process %ld\n",p-proc);
  }
  assert(p == NULL, "#2 - No runnable process: should return NULL", STOP);
  assert(prio_lock.num == 0, "#2 - No runnable process: should not hold prio_lock", STOP);
  int numlocksheld = 0;
  for(i = 0; i < NPROC; i++){
    char* fmt = "#2 - No runnable process: should not hold lock on process %d";
    int n = snprintf(NULL, 0, fmt, i);
    char * s = malloc(n+1);
    snprintf(s, n+1, fmt, i);
    numlocksheld += (proc[i].lock.num != 0);
    free(s);
  }
  assert(numlocksheld == 0, "#2 - No runnable process: should not hold lock on any process\n", NOSTOP);
}

void test_one_runnable_process(){
  int i;
  struct proc* p;
  for(i = 0; i < NPROC; i++){
    proc[i].state = UNUSED;
    proc[i].priority = 5;
    acquire(&prio_lock);
    acquire(&proc[i].lock);
    insert_into_prio_queue(&proc[i]);
    release(&proc[i].lock);
    release(&prio_lock);
  }
  proc[3].state = SLEEPING;
  proc[4].state = RUNNABLE;
  printf("### All priority queues are empty, except :\n");
  printf(" priority 5: contains all processes in state UNUSED\n");
  printf(" except process 3 with state SLEEPING\n");
  printf(" and    process 4 with state RUNNABLE\n");
  printf("Calling pick_highest_priority_runnable_proc\n");

  p = pick_highest_priority_runnable_proc();
  if(!p)
    printf("returned NULL\n");
  else{
    printf("returned process %ld\n",p-proc);
  }
  assert(p == &proc[4], "#3 - One runnable process: should return process 4", STOP);
  assert(p->lock.num == 1, "#3 - One runnable process: should hold process 4's lock", STOP);
  assert(prio_lock.num == 1, "#3 - One runnable process: should hold prio_lock", STOP);
  release(&proc[4].lock);
  release(&prio_lock);
}



void test_two_runnable_processes(){
  int i;
  struct proc* p;
  for(i = 0; i < NPROC; i++){
    proc[i].state = UNUSED;
    proc[i].priority = 5;
    acquire(&prio_lock);
    acquire(&proc[i].lock);
    insert_into_prio_queue(&proc[i]);
    release(&proc[i].lock);
    release(&prio_lock);
  }
  proc[3].state = RUNNABLE;
  proc[4].state = RUNNABLE;

  acquire(&prio_lock);
  acquire(&proc[3].lock);
  remove_from_prio_queue(&proc[3]);
  proc[3].priority = 7;
  insert_into_prio_queue(&proc[3]);
  release(&proc[3].lock);
  release(&prio_lock);

  printf("### All priority queues are empty, except :\n");
  printf(" priority 5: contains all processes in state UNUSED\n");
  printf(" except process 3 with state RUNNABLE and priority 7\n");
  printf(" and    process 4 with state RUNNABLE and priority 5\n");
  printf("Calling pick_highest_priority_runnable_proc\n");

  p = pick_highest_priority_runnable_proc();
  if(!p)
    printf("returned NULL\n");
  else{
    printf("returned process %ld\n",p-proc);
  }
  assert(p == &proc[4], "#4 - Two runnable processes: should return process with highest priority", STOP);
  assert(p->lock.num == 1, "#4 - Two runnable processes: should hold process 4's lock", STOP);
  assert(prio_lock.num == 1, "#4 - Two runnable processes: should hold prio_lock", STOP);
}

void reinit_prio_queues(){
  for(int i = 0; i < NPRIO; i++){
    prio[i] = NULL;
  }
}

int main(){

  initator();

  reinit_prio_queues();
  // Test 1 : no process in queues
  test_no_process();

  reinit_prio_queues();
  // Test 2 : only non-runnable processes in queues
  test_no_runnable_process();

  reinit_prio_queues();
  // Test 3 : only one runnable process
  test_one_runnable_process();

  reinit_prio_queues();
  // Test 4 : several runnable processes
  test_two_runnable_processes();


  return 0;
}
