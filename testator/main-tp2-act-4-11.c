#include <stdio.h>
#include "stubs2.h"
#include "fat.h"

pte_t my_pte[10];

pte_t* walk(pagetable_t pt, uint64 va, int alloc){
  int idx = va >> 12;
  if (idx < 10)
    return &my_pte[idx];
  return NULL;
}

int main(){

  install_segv_handler();

  pagetable_t pt;
  int res;
  char* str;
  struct proc p;
  memset(&p, 0, sizeof(p));
  set_myproc(&p);

  add_memory_area(&p, 0x1000, 0x6000);

  has_panicked = 0;
  res = do_allocate_range(pt, myproc(), 0, 0x2000);

  str = easy_snprintf("do_allocate_range should return ENOVMA (got %d) on address outside VMA", res);
  assert(!has_panicked && res == ENOVMA, str, NOSTOP);
  free(str);
  str = NULL;

  has_panicked = 0;
  res = do_allocate_range(pt, myproc(), 0x1000, 0x3000);
  assert(!has_panicked && res == 0,
         "do_allocate_range should return 0 on address at start of a VMA",
         NOSTOP);

  has_panicked = 0;
  res = do_allocate_range(pt, myproc(), 0x2000, 0x5000);
  str = easy_snprintf("do_allocate_range should return ENOVMA on range overflowing out of a VMA");
  assert(!has_panicked && res == ENOVMA,
         str,
         NOSTOP);
  free(str);
  str = NULL;

  for (int i = 0; i < 10; i++) my_pte[i] = 0;

  has_panicked = 0;
  res = do_allocate_range(pt, myproc(), 0x1000, 0x4000);
  assert(!has_panicked && res == 0, "do_allocate_range should return 0 on in-VMA address", NOSTOP);

  pte_t* pte;
  struct {uint64 va; int v;} tests[] = {
    {0x0000, 0},
    {0x1000, 1},
    {0x2000, 1},
    {0x3000, 1},
    {0x4000, 1},
    {0x5000, 0},
    {0x6000, 0},
    {0x7000, 0},
  };
  for(int i = 0; i < sizeof(tests)/sizeof(tests[0]); i++){
    has_panicked = 0;
    pte = walk(pt, tests[i].va, 0);
    str = easy_snprintf("do_allocate_range: for VA=%llx, pte=%lu, V=%d (expected %d)\n", tests[i].va, pte, pte ? ((*pte & PTE_V)): 0, tests[i].v);
    assert(!has_panicked && pte != 0 && ((*pte & PTE_V) == tests[i].v), str, NOSTOP);
    free(str);
  }


  return 0;
}
