from typing import List
from clang.cindex import Cursor, Index, CursorKind, TypeKind

def get_var_decls(path):
    i = Index.create()
    tu = i.parse(path)
    funcs = []
    filename = tu.cursor.spelling
    for c in tu.cursor.walk_preorder():
        if c.location.file is None:
            pass
        elif c.location.file.name != filename:
            pass
        elif c.kind == CursorKind.VAR_DECL:
            funcs.append(c)
    return funcs



def get_functions(path):
    i = Index.create()
    tu = i.parse(path)
    funcs = []
    filename = tu.cursor.spelling
    for c in tu.cursor.walk_preorder():
        if c.location.file is None:
            pass
        elif c.location.file.name != filename:
            pass
        elif c.kind == CursorKind.FUNCTION_DECL:
            funcs.append(c)
    return funcs


def get_funcalls(cursor):
    funcs = []
    for c in cursor.walk_preorder():
        if c.kind == CursorKind.CALL_EXPR:
            funcs.append(c)
    return funcs


def get_structs(path):
    i = Index.create()
    tu = i.parse(path)
    structs = []
    filename = tu.cursor.spelling
    for c in tu.cursor.walk_preorder():
        if c.location.file is None:
            pass
        elif c.location.file.name != filename:
            pass
        elif c.kind == CursorKind.STRUCT_DECL:
            structs.append(c)
    return structs


# https://stackoverflow.com/a/66371890/2357463
def code_from_cursor(cursor: Cursor) -> List[str]:
    code = []
    line = ""
    prev_token = None
    for tok in cursor.get_tokens():
        if prev_token is None:
            prev_token = tok
        prev_location = prev_token.location
        prev_token_end_col = prev_location.column + len(prev_token.spelling)
        cur_location = tok.location
        if cur_location.line > prev_location.line:
            code.append(line)
            line = " " * (cur_location.column - 1)
        else:
            if cur_location.column > (prev_token_end_col):
                line += " "
        line += tok.spelling
        prev_token = tok
    if len(line.strip()) > 0:
        code.append(line)
    return code

def get_function_by_name(path, fname):
    funcs = get_functions(path)
    c = [c for c in funcs if c.spelling == fname]
    if c == []:
        return None
    l = [x for x in c if x.is_definition()]
    if len(l) > 0: return l[0]
    return c[0]


def get_struct_by_name(path, sname):
    structs = get_structs(path)
    c = [c for c in structs if c.spelling == sname and c.is_definition()]
    if c == []:
        return None
    else:
        return c[0]


def get_var_decl_by_name(path, sname):
    var_decls = get_var_decls(path)
    c = [c for c in var_decls if c.spelling == sname]
    if c == []:
        return None
    else:
        return c[0]


def get_funbody_from_cursor(f):
    if f:
        body = code_from_cursor(f)
        return '\n'.join(body)
    else:
        return None

def get_proto_from_cursor(f):
    rettype = f.result_type.spelling
    fname = f.spelling
    i = 0
    args = []
    for arg in f.get_arguments():
        args.append((arg.type.spelling, arg.spelling))
        i+=1
    s = "{} {}({});".format(rettype, fname, ", ".join(map(lambda tn: "{} {}".format(tn[0],tn[1]), args)))
    return s

def get_funbody(path, fname):
    return get_funbody_from_cursor(get_function_by_name(path, fname))

def get_proto(path, fname):
    f = get_function_by_name(path, fname)
    return get_proto_from_cursor(f)

def get_enum_decl_cases(e):
    return [x.spelling for x in e.get_children()]

def get_struct_fields(s):
    return list(s.get_children())

def get_var_array_type(s):
    if s.type.kind != TypeKind.CONSTANTARRAY:
        return None
    else:
        return (s.type.element_type.spelling, s.type.element_count)

def get_fun_args(f):
    args = []
    for x in f.get_arguments():
        args.append((x.type.spelling, x.spelling))
    return args

