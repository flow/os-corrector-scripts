import requests
from packaging import version
import argparse
import tarfile, io
import sys

def get_cur_ver():
    ver = None
    with open("VERSION") as f:
        for line in f:
            ver = line.rstrip()
            ver = version.parse(ver)
            break
    return ver


def get_available_versions():
    ver = get_cur_ver()
    print("Version = '{}'".format(ver))
    try:
        print("Checking for new versions...")
        r = requests.get("https://gitlab-research.centralesupelec.fr/api/v4/projects/cidre-public%2Fsystemes-exploitation%2Fos-corrector-scripts/releases", timeout=1)
        print("Got list of versions.")
        j = r.json()

        bestver = ver
        url = None

        for release in j:
            rver = version.parse(release['name'])
            if rver > bestver: 
                bestver = rver
                url = release['assets']['links'][0]['url']
        print("Latest version is {}".format(bestver))
        return (ver, bestver, url)
    except requests.ConnectionError:
        print("Could not fetch available versions (probably DNS, or connection error)")
        return (ver, ver, None)
    except HTTPError:
        print("Could not fetch available versions (HTTP error)")
        return (ver, ver, None)
    except Timeout:
        print("Could not fetch available versions (timeout)")
        return (ver, ver, None)



ver,bestver,url = get_available_versions()

if url != None:
    print("Updating from {} to {}".format(ver, bestver))
    print("You might want to `docker pull pwilke/oscorrector` to speed up testing!")
    r = requests.get(url, stream=True)
    if r.status_code != 200:
        print("Could not fetch release {}".format(bestver))
    else:
        print("Fetched release {}".format(bestver))
    byte_array = r.raw.read()
    flo = io.BytesIO(byte_array)
    f = tarfile.open(fileobj=flo)
    f.extractall('.')
    f.close()
    print("Finished uncompressing release {}".format(bestver))
    
else:
    print("Already up to date")

sys.argv = ["corrector.py", "--repo", "/data", "--step"] + sys.argv[1:]
import corrector
corrector.main()
