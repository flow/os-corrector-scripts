from time import sleep
import pexpect
import re
from git import Repo, BadName, GitCommandError
from unidiff import PatchSet
import sys
import subprocess
import tempfile
#from distutils.dir_util import copy_tree
from shutil import copy2, rmtree
import argparse
import coloredlogs, logging
import pdb
from result import Ok, Err, Result

logger = logging.getLogger("corrector")
coloredlogs.install(level='DEBUG',
                    fmt='%(asctime)s %(levelname)s %(message)s',
                    logger=logger)

import parsor
from assertator import *

ncpus = 3
checkout = False
checkout_commit = None
repopath = None
keep_tmps = False
git_branch_renamings = dict()

def git_rename_branch(branch):
    global git_branch_renamings
    if branch in git_branch_renamings:
        branch = git_branch_renamings[branch]
    return branch

def git_register_renaming(b1, b2):
    global git_branch_renamings
    git_branch_renamings[b1] = b2

def get_repopath():
    if not repopath:
        logger.critical("Where is the repo? Please pass argument --repo to main.")
        exit(-1)
    return repopath


def run(cmd, show_output=False, timeout=15, cwd=None):
    try:
        cp = subprocess.run(cmd, capture_output=show_output, timeout=timeout, cwd=cwd)
        out, err = cp.stdout, cp.stderr
        out = out.decode('utf8') if out is not None else None
        err = err.decode('utf8') if err is not None else None
        return (out, err, cp.returncode, False)
    except subprocess.TimeoutExpired as inst:
        out = inst.stdout.decode('utf8') if inst.stdout is not None else None
        err = inst.stderr.decode('utf8') if inst.stderr is not None else None
        return (out, err, None, True)

def run_res(cmd, show_output=False, timeout=15, cwd=None, check_error=False):
    r = run(cmd, show_output=show_output, timeout=timeout, cwd=cwd)
    out, err, code, timeout = r
    if timeout:
        return Err("Command '{}' timed out.\nOut: {}\nErr:{}\n"
                   .format(" ".join(cmd),
                           out,
                           err))

    if check_error and code != 0:
        return Err("Command '{}' exited with code {}.\nError:\n{}\n".
                   format(" ".join(cmd),
                          code,
                          err
                          )
                   )
    return Ok((out, err))

class UnitTest():
    def __init__(self, srcdir, at):
        self.srcdir = srcdir
        self.at = at
        self.paths = []
        self.funcs = []
        self.fatcfile = "fat.c"
        self.fathfile = "fat.h"
        self.stubname = "stubs"
        self.extra_files = []
        self.tmpdir = tempfile.mkdtemp()
        self.errors = []

    def clean(self):
        if keep_tmps:
            logger.info("Kept tmp files at {}".format(self.tmpdir))
        else:
            rmtree(self.tmpdir)

    def add_source_fun(self, srcfile, fname, onlyproto=False):
        elt = (get_repopath() + "/" + srcfile, fname, 'onlyproto' if onlyproto else '')
        self.funcs.append(elt)

    def add_extra_file(self, filename, dst):
        self.extra_files.append((filename, dst))

    def add_stub_file(self, filename):
        self.paths.append(filename)

    def stubsdef(self):
        funcs = parsor.get_functions("{}/{}.h".format(self.tmpdir, self.stubname))
        return [fn.spelling for fn in funcs]

    def populate_tmpdir(self):
        for p in self.paths:
            copy2(self.srcdir+"/"+p, self.tmpdir + "/" + p)
        for src, dst in self.extra_files:
            copy2(src, self.tmpdir + "/" + dst)

    def accumulate_func_deps(self):
        # visited and funcs are lists of pairs (srcfile, function name, proto, funbody)
        visited = []
        funcs = list(self.funcs)
        stubsdef = self.stubsdef()
        for x in funcs:
            stubsdef.append(x[1])
        while len(funcs) != 0:
            fun = funcs.pop()
            if fun[1] in map(lambda x: x[1], visited):
                continue
            srcfile, fname = fun[0], fun[1]
            func_cursor = parsor.get_function_by_name(srcfile, fname)
            if func_cursor is None:
                self.errors.append("expected to find function {} in {}".format(fname, srcfile))
                continue
            onlyproto = len(fun) > 2 and fun[2] == 'onlyproto'
            res = (srcfile, fname,
                   parsor.get_proto_from_cursor(func_cursor),
                   parsor.get_funbody_from_cursor(func_cursor) if not onlyproto else None)
            visited.append(res)
            if not onlyproto:
                fcalls = [c.spelling for c in parsor.get_funcalls(func_cursor) if c.spelling not in stubsdef]
                for f in fcalls:
                    # we assume the dependent functions are in the same file...
                    funcs.append((srcfile, f))
        self.funcs = visited

    def write_c_function(self, fbody, f):
        if fbody is None: return
        fbody = fbody.replace("static", "")
        f.write(fbody)
        f.write("\n\n")
        
    def write_c_file(self):
        with open("{}/{}".format(self.tmpdir, self.fatcfile), "w") as f:
            f.write("#include \"{}.h\"\n".format(self.stubname))
            f.write("#include \"fat.h\"\n\n")
            for fun in self.funcs:
                self.write_c_function(fun[3], f)

    def write_h_function(self, fbody, f):
        f.write(fbody + "\n")
        
    def write_h_file(self):
        with open("{}/{}".format(self.tmpdir, self.fathfile), "w") as f:
            f.write("#ifndef __FAT_H\n")
            f.write("#define __FAT_H\n")
            f.write("#include \"{}.h\"\n".format(self.stubname))
            for fun in self.funcs:
                self.write_h_function(fun[2], f)
            f.write("\n\n#endif\n")

    def compile(self) -> Result[bool, str]:
        logger.info("Compiling")
        paths = self.paths + [self.fatcfile]
        cfiles = ["{}/{}".format(self.tmpdir,f) for f in paths if f.endswith(".c")]
        cfiles = " ".join(cfiles)
        compile_cmd = "clang {} -g -rdynamic -Wno-invalid-noreturn -ldl -o {}/testator".format(cfiles, self.tmpdir).split(' ')
        res = run_res(compile_cmd, show_output=True, check_error=True)
        # logger.info("Compiling res = {}".format(res))
        if isinstance(res, Err):
            logger.critical("Got error: {}".format(res))
            return res
        else:
            if res.value[1]:
                print(res.value[1])
        return Ok(True)

    def run(self):
        logger.info("Running")
        run_cmd = [self.tmpdir+"/testator"]
        res = run_res(run_cmd, show_output=True, cwd=self.tmpdir)
        # logger.info("Running res = {}".format(res))
        if isinstance(res, Err):
            logger.critical("Got error: {}".format(res))
            self.at.record_test((False, res.value, None))
            return
        out, _ = res.value
        out = out.splitlines()
        for l in out:
            if l.startswith("OK "):
                self.at.record_test((True, l[3:], None))
            elif l.startswith("KO "):
                self.at.record_test((False, l[3:], None))
            else:
                logger.info("{}".format(l))

    def go(self):
        self.populate_tmpdir()
        self.accumulate_func_deps()
        if len(self.errors) > 0:
            for err in self.errors:
                self.at.record_test((False, err, None))
        else:
            self.write_h_file()
            self.write_c_file()
            r = self.compile()
            if isinstance(r, Err):
                self.at.record_test((False, r.value, None))
            self.run()
        self.clean()

class Executator():

    def __init__(self):
        rp = get_repopath()
        kernpath = "{}/kernel/kernel".format(rp)
        fsimgpath = "{}/fs.img".format(rp)
        cmd = "qemu-system-riscv64 "
        cmd += "-machine virt -bios none "
        cmd += "-kernel {} ".format(kernpath)
        cmd += "-m 128M "
        cmd += "-smp {} ".format(ncpus)
        cmd += "-nographic "
        cmd += "-drive file={},if=none,format=raw,id=x0 ".format(fsimgpath)
        cmd += "-device virtio-blk-device,drive=x0,bus=virtio-mmio-bus.0 "
        #cmd += "-monitor telnet:127.0.0.1:12345,server,nowait"
        self.child = pexpect.spawn(cmd)
        self.booted = False
        self.console_initialized = False
        self.booted = self._expect("\$ ")
        self.bootmsg = self.child.before.decode('utf8')
        if not self.booted:
            return
        self.console_initialized = self.init_consoles()

    def _expect(self, pat, timeout=10):
        try:
            self.child.expect(pat, timeout=timeout)
            return True
        except pexpect.exceptions.TIMEOUT:
            return False

    def _send(self, cmd):
        self.child.send(cmd)

    def _sendline(self, cmd):
        self.child.sendline(cmd)

    def init_consoles(self):
        self._send("\x13")
        if not self._expect("\$ "): return False
        self._send("\x13")
        if not self._expect("\$ "): return False
        self._send("\x13echo\n")
        if not self._expect("\$ "): return False
        return True


    def expect(self, pat, timeout=3):
        if not self.booted:
            logger.critical("Make sure machine is booted before using expect.")
            return False
        if not self.console_initialized:
            logger.critical("Make sure consoles are initialized before using expect.")
            return False
        return self._expect(pat, timeout)

    def send(self, cmd):
        if not self.booted:
            logger.critical("Make sure machine is booted before using send.")
            return False
        if not self.console_initialized:
            logger.critical("Make sure consoles are initialized before using send.")
            return False
        self._send(cmd)

    def sendline(self, cmd):
        if not self.booted:
            logger.critical("Make sure machine is booted before using sendline.")
            return False
        if not self.console_initialized:
            logger.critical("Make sure consoles are initialized before using sendline.")
            return False
        self._sendline(cmd)

    def get_output(self):
        bef = None
        aft = None
        if self.child.before:
            if self.child.before != pexpect.exceptions.TIMEOUT:
                bef = self.child.before.decode('utf8')
        if self.child.after:
            if self.child.after != pexpect.exceptions.TIMEOUT:
                aft = self.child.after.decode('utf8')
        return bef, aft

    def get_list_proc(self):
        self.sendline("\x10echo")
        self.expect("\$ ")
        bef, aft = self.get_output()
        lines = bef.splitlines()
        lproc = []
        for l in lines:
            if l == '': continue
            if l == 'echo': break
            cols = l.split("\t")
            if cols[0] == 'PID': continue
            lproc.append({
                'pid': int(cols[0]),
                'ppid': int(cols[1]),
                'priority': int(cols[2]),
                'state': cols[3],
                'cmd': cols[4]
            })
        return lproc

    def get_prio_queues(self):
        self.sendline("\x11echo")
        self.expect("\$ ")
        bef, _ = self.get_output()
        lines = bef.splitlines()
        queues = dict()
        for l in lines:
            if l == '': continue
            if l == 'echo': break
            m = re.match(r'Priority queue for priority = ([0-9]): (.*)', l)
            if m:
                queues[int(m.group(1))] = list(map(lambda x: int(x),
                                              filter(lambda x: x != '',m.group(2).split(' '))))
            else:
                logger.critical("Error '{}'".format(l))
                # exit(-1)
        return queues

    def ls_output(self):
        self.sendline("ls")
        self.expect("\$ ")
        bef, _ = self.get_output()
        lines = bef.splitlines()
        out = []
        for l in lines[1:]:
            m = re.match(r'([^ ]*) *(\d+) (\d+) (\d+)', l)
            if m:
                out.append({
                    'name': m.group(1),
                    'major': int(m.group(2)),
                    'minor': int(m.group(3)),
                    'size': int(m.group(4))
                })
            else:
                logger.warning("NOMATCH '{}'".format(l))
        return out




class Testator():
    def __init__(self, executator, quiet=False):
        self.executator = executator
        self.tests = []
        self.quiet = quiet

    def record_test(self, testresult):
        self.tests.append(testresult)
        if not self.quiet:
            ok, descr, info = testresult
            okko = "OK" if ok else "KO"
            if not ok:
                if info:
                    logger.error("{} {}: {}".format(okko, descr, info))
                else:
                    logger.error("{} {}".format(okko, descr))
            else:
                logger.debug("{} {}".format(okko, descr))

    def print_summary(self):
        n = len(self.tests)
        nok = len([t for t in self.tests if t[0]])
        nko = n - nok
        logger.info("Ran {} tests : {} OK, {} KO".format(n, nok, nko))

    def test_booted(self):
        self.record_test((self.executator.booted, "booted", None))
        return self.executator.booted

    def test_not_booted(self):
        self.record_test((not self.executator.booted, "not booted", None))
        return not self.executator.booted

    def test_console_initialized(self):
        self.record_test((self.executator.console_initialized, "consoles initialized", None))
        return self.executator.console_initialized

    def test_list_procs(self, exp):
        # List processes
        procs = self.executator.get_list_proc()
        lpids = [x['pid'] for x in procs]
        self.record_test(test_eq_list(lpids, exp, "list procs"))

    def test_prio_queues(self, fexp, sameorder=True):
        queues = self.executator.get_prio_queues()
        self.record_test(test_dict(queues, range(0,10), fexp, "prio queues", sameorder=sameorder))

    # TODO check this test?
    def test_nice(self):
        queues = self.executator.get_prio_queues()
        pid = None
        prio = None
        for p in queues:
            if queues[p] != []:
                pid = queues[p][0]
                prio = p
                break
        if pid is not None:
            newprio = 9 - prio
            self.executator.sendline("nice {} {}".format(pid, newprio))
            self.executator.expect("\$ ")
            queues = self.executator.get_prio_queues()
            ok = pid in queues.get(newprio, [])
            self.record_test((ok, "nice", "{}".format(queues)))
        else:
            self.record_test((False, "nice", "No pid in queues?"))


    def test_mutest(self):
        self.executator.sendline("mutest")
        self.executator.expect("\$ ")
        bef, _ = self.executator.get_output()
        last = bef.splitlines()[-1]
        self.record_test((last == "Oui, père.", "mutest should end with 'Oui, père.'", None))

    def test_mutest2(self):
        self.executator.sendline("mutest2")
        self.executator.expect("\$ ", timeout=10)
        bef, _ = self.executator.get_output()
        last = bef.splitlines()[-1]
        self.record_test((last == "Result = 100", "mutest2 should end with 'Result = 100'", "got '{}'".format(last)))


    def test_num_files(self, num):
        lso = self.executator.ls_output()
        self.record_test(test_eq_int(len(lso), num, "num files {}".format(lso)))

    def test_prints(self, keyshere, keysnothere):
        self.executator.sendline("prints")
        sleep(2)
        self.executator.sendline("\x13echo")
        self.executator.expect("\$ ")
        bef, _ = self.executator.get_output()
        if verbose:
            print(bef)
        counts = dict()
        keys = keyshere + keysnothere
        for k in keys:
            counts[k] = 0
        for line in bef.splitlines():
            if len(line) >= 2:
                if line[0] in keys and line[1] == ' ':
                    counts[line[0]]+=1
        vhere = [counts[k] for k in keyshere]
        avg = sum(vhere)/len(vhere)
        bad = []
        for k in keyshere:
            if abs(avg - counts[k]) > 0.25 * avg:
                bad.append("  Expected more of '{}' (got {}, counts={})!".format(k, counts[k], counts))
        for k in keysnothere:
            if counts[k] != 0:
                bad.append("  Should be no {} at all, got {}".format(k, counts[k]))
        res = (True, "test prints", None)
        if bad != []:
            res = (False, "test prints", "\n" + "\n".join(bad))
        self.record_test(res)



    def test_prints_kill(self, keyshere, keysnothere, kill=None):
        self.executator.sendline("prints")
        sleep(2)
        self.executator.sendline("\x13echo")
        self.executator.expect("\$ ")
        if kill:
            procs = self.executator.get_list_proc()
            p = [p for p in procs if p['cmd'] == "'print {}'".format(kill)]
            if p == []:
                logger.critical("Could not find desired process: got {}".format([p['cmd'] for p in procs]))
                return
            pid = p[0]['pid']
            logger.info("Killing process {}".format(pid))
            self.executator.sendline("kill {}".format(pid))
            self.executator.sendline("\x13\x13echo")
            self.executator.expect("\$ ")
            sleep(2)
            self.executator.sendline("\x13echo")
            self.executator.expect("\$ ")
        bef, _ = self.executator.get_output()
        if verbose:
            print(bef)
        counts = dict()
        keys = keyshere + keysnothere
        for k in keys:
            counts[k] = 0
        for line in bef.splitlines():
            if len(line) >= 2:
                if line[0] in keys and line[1] == ' ':
                    counts[line[0]]+=1
        vhere = [counts[k] for k in keyshere]
        avg = sum(vhere)/len(vhere)
        bad = []
        for k in keyshere:
            if abs(avg - counts[k]) > 0.25 * avg:
                bad.append("Expected more of '{}' (got {}, counts={})!".format(k, counts[k], counts))
        for k in keysnothere:
            if counts[k] > 1:
                # there can be one that took place before switching consoles
                bad.append("Should be no {} at all, got {}".format(k, counts[k]))
        res = (True, "test prints", None)
        if bad != []:
            res = (False, "test prints", "\n" + "\n".join(bad))
        self.record_test(res)

    def test_prints_nice(self, keyshere, keysnothere, nice=None):
        self.executator.sendline("prints")
        sleep(2)
        self.executator.sendline("\x13echo")
        self.executator.expect("\$ ")
        if nice:
            procs = self.executator.get_list_proc()
            p = [p for p in procs if p['cmd'] == "'print {}'".format(nice)]
            if p == []:
                logger.critical("Could not find desired process: got {}".format([p['cmd'] for p in procs]))
                return
            pid = p[0]['pid']
            logger.info("Niceing process {}".format(pid))
            self.executator.sendline("nice {} 9".format(pid))
            self.executator.sendline("\x13\x13echo")
            self.executator.expect("\$ ")
            sleep(2)
            self.executator.sendline("\x13echo")
            self.executator.expect("\$ ")
        bef, _ = self.executator.get_output()
        if verbose:
            print(bef)
        counts = dict()
        keys = keyshere + keysnothere
        for k in keys:
            counts[k] = 0
        for line in bef.splitlines():
            if len(line) >= 2:
                if line[0] in keys and line[1] == ' ':
                    counts[line[0]]+=1
        vhere = [counts[k] for k in keyshere]
        avg = sum(vhere)/len(vhere)
        bad = []
        for k in keyshere:
            if abs(avg - counts[k]) > 0.25 * avg:
                bad.append("Expected more of '{}' (got {}, counts={})!".format(k, counts[k], counts))
        for k in keysnothere:
            if counts[k] > 1:
                # there can be one that took place before switching consoles
                bad.append("Should be no {} at all, got {}".format(k, counts[k]))
        res = (True, "test prints", None)
        if bad != []:
            res = (False, "test prints", "\n" + "\n".join(bad))
        self.record_test(res)

def get_git_repo():
    repo = Repo(get_repopath())
    return repo

def checkout_and_remake(branch):
    if checkout:
        repo = get_git_repo()
        if checkout_commit:
            try:
                repo.git.checkout(checkout_commit)
            except GitCommandError:
                return ("Cannot checkout commit {}".format(checkout_commit))
        else:
            branch = git_rename_branch(branch)
            try:
                repo.git.checkout(branch)
            except GitCommandError:
                return ("Cannot checkout branch {}".format(branch))

    out, err, code, _ = run(["rm", "-f", "{}/fs.img".format(get_repopath())], show_output=True)
    if code != 0:
        return ("Cannot rm fs.img\nError:\n{}\n".format(err))
    out, err, code, _ = run(["make", "-C", get_repopath(), "clean"], show_output=True)
    if code != 0:
        return ("Cannot clean repo\nError:\n{}\n".format(err))
    out, err, code, _ = run(["make", "-C", get_repopath(), "fs.img"], show_output=True)
    if code != 0:
        return ("Cannot compile fs.img\nError:\n{}\n".format(err))
    out, err, code, _ = run(["make", "-C", get_repopath()],show_output=True)
    if code != 0:
        return ("Cannot compile kernel\nError:\n{}\n".format(err))
    return None

tests = dict()

class ActivityTestor(Testator):
    def __init__(self, branch):
        err = checkout_and_remake(branch)
        if err:
            super().__init__(None)
            self.record_test((0, err, None))
            exit(-1)
        self.executator = Executator()
        super().__init__(self.executator)

def tptest(name):
    def decorator(func):
        tests[name] = func
        return func
    return decorator

@tptest('tp1-init')
def tp1_init():
    at = ActivityTestor('tp1-init')
    at.test_booted()
    at.test_console_initialized()
    at.test_list_procs([1,2,3,4,5,6,7])
    at.test_prio_queues(lambda x: [])
    at.print_summary()

@tptest('tp1-act-1-2')
def tp1_act_1_2():
    global ncpus
    ncpus = 4
    at = ActivityTestor('tp1-act-1-2')
    if at.test_booted():
        if at.test_console_initialized():
            at.test_list_procs([1,2,3,4,5,6,7])
            at.test_prio_queues(lambda x: [1,2,3,4,5,6,7] if x == 5 else [])
            at.test_num_files(29)
            at.test_prints("ABCD", "")

    ut = UnitTest('testator', at)
    ut.stubname = "stubs1"
    ut.add_stub_file('stubs1.c')
    ut.add_stub_file('stubs1.h')
    ut.add_stub_file('main-tp1-act-1-2.c')
    ut.add_source_fun('kernel/proc.c', 'sleep', onlyproto=True)
    ut.add_source_fun('kernel/proc.c', 'sched', onlyproto=True)
    ut.add_source_fun('kernel/proc.c', 'allocproc')
    ut.add_source_fun('kernel/proc.c', 'freeproc')
    ut.add_source_fun('kernel/proc.c', 'userinit')
    ut.add_source_fun('kernel/proc.c', 'fork')
    ut.add_source_fun('kernel/proc.c', 'exit')
    ut.add_source_fun('kernel/proc.c', 'wait')
    ut.add_source_fun('kernel/proc.c', 'kill')

    ut.go()
    at.print_summary()

@tptest('tp1-act-2-1')
def tp1_act_2_1():
    # TODO: stubs : vérifier l'ordre des verrous
    global ncpus
    ncpus = 4
    at = ActivityTestor('tp1-act-2-1')
    if at.test_booted():
        if at.test_console_initialized():
            at.test_list_procs([1,2,3,4,5,6,7])
            at.test_prio_queues(lambda x: [1,2,3,4,5,6,7] if x == 5 else [])
            at.test_num_files(29)
            at.test_prints("ABCD", "")
    proccpath = "{}/kernel/proc.c".format(get_repopath())
    f = parsor.get_function_by_name(proccpath, 'pick_highest_priority_runnable_proc')
    test_not_none(f, "presence of function pick_highest_priority_runnable_proc")

    ut = UnitTest('testator', at)
    ut.stubname = "stubs1"
    ut.add_stub_file('stubs1.c')
    ut.add_stub_file('stubs1.h')
    ut.add_stub_file('main-tp1-act-2-1.c')
    ut.add_source_fun('kernel/proc.c', 'pick_highest_priority_runnable_proc')
    ut.go()
    at.print_summary()

@tptest('tp1-act-2-2')
def tp1_act_2_2():
    at = ActivityTestor('tp1-act-2-2')
    at.test_booted()
    at.test_console_initialized()
    at.test_list_procs([1,2,3,4,5,6,7])
    at.test_prio_queues(lambda x: [1,2,3,4,5,6,7] if x == 5 else [], sameorder=False)
    at.test_num_files(29)
    at.test_prints_kill("ABC", "D", kill="D")
    proccpath = "{}/kernel/proc.c".format(get_repopath())
    f = parsor.get_function_by_name(proccpath, 'pick_highest_priority_runnable_proc')
    at.record_test(test_not_none(f, "presence of function pick_highest_priority_runnable_proc"))

    ut = UnitTest('testator', at)
    ut.stubname = "stubs1"
    ut.add_stub_file('stubs1.c')
    ut.add_stub_file('stubs1.h')
    ut.add_stub_file('main-tp1-act-2-2.c')
    ut.add_source_fun('kernel/proc.c', 'pick_highest_priority_runnable_proc')
    ut.add_source_fun('kernel/proc.c', 'scheduler')
    ut.go()

    at.print_summary()


@tptest('tp1-act-3-1')
def tp1_act_3_1():
    at = ActivityTestor('tp1-act-3-1')
    at.test_booted()
    at.test_console_initialized()
    proccpath = "{}/kernel/proc.c".format(get_repopath())
    f = parsor.get_function_by_name(proccpath, "nice")
    at.record_test(test_not_none(f, "presence of function nice"))

    ut = UnitTest('testator', at)
    ut.stubname = "stubs1"
    ut.add_stub_file('stubs1.c')
    ut.add_stub_file('stubs1.h')
    ut.add_stub_file('main-tp1-act-3-1.c')
    ut.add_source_fun('kernel/proc.c', 'nice')
    ut.go()

    at.print_summary()


@tptest('tp1-act-3-2')
def tp1_act_3_2():
    at = ActivityTestor('tp1-act-3-2')
    at.test_booted()
    at.test_console_initialized()
    sysproccpath = "{}/kernel/sysproc.c".format(get_repopath())
    proccpath = "{}/kernel/proc.c".format(get_repopath())
    f = parsor.get_function_by_name(sysproccpath, "sys_nice")
    at.record_test(test_not_none(f, "presence of function sys_nice"))

    ut = UnitTest('testator', at)
    ut.stubname = "stubs1"
    ut.add_stub_file('stubs1.c')
    ut.add_stub_file('stubs1.h')
    ut.add_stub_file('main-tp1-act-3-2.c')
    ut.add_source_fun('kernel/proc.c', 'nice')
    ut.add_source_fun('kernel/sysproc.c', 'sys_nice')
    ut.go()
    
    at.print_summary()


@tptest('tp1-act-3-3')
def tp1_act_3_3():
    at = ActivityTestor('tp1-act-3-3')
    at.test_booted()
    at.test_console_initialized()
    at.test_nice()
    at.test_prints_nice("ABC", "D", nice="D")

    proccpath = "{}/kernel/proc.c".format(get_repopath())
    f = parsor.get_function_by_name(proccpath, "nice")
    at.record_test(test_not_none(f, "presence of function nice"))

    ut = UnitTest('testator', at)
    ut.stubname = "stubs1"
    ut.add_stub_file('stubs1.c')
    ut.add_stub_file('stubs1.h')
    ut.add_stub_file('main-tp1-act-3-3.c')
    ut.add_source_fun('kernel/proc.c', 'nice')
    ut.go()

    at.print_summary()

from clang.cindex import Cursor, Index, CursorKind

@tptest('tp1-act-4-3')
def tp1_act_4_3():
    at = ActivityTestor('tp1-act-4-3')
    at.test_booted()
    at.test_console_initialized()

    filehpath = "{}/kernel/file.h".format(get_repopath())
    s = parsor.get_struct_by_name(filehpath, "file")
    at.record_test(test_not_none(s, "presence of struct file"))
    fields = parsor.get_struct_fields(s)
    enum = [f for f in fields if f.spelling == '' and f.kind == CursorKind.ENUM_DECL]
    at.record_test((len(enum) == 1, "presence of enum type in struct file (should be exactly one)", "got {}".format(len(enum))))
    if len(enum) > 0:
        enum = enum[0]
        cases = parsor.get_enum_decl_cases(enum)
        exp_cases = ['FD_PIPE', 'FD_MUTEX', 'FD_NONE', 'FD_INODE', 'FD_DEVICE']
        at.record_test(test_eq_list(cases, exp_cases, "enum fields", sameorder=False))
    mutex_field = [f for f in fields if f.kind == CursorKind.FIELD_DECL and f.type.spelling == "struct sleeplock"]
    at.record_test((len(mutex_field) == 1, "presence of mutex in struct file (should be exactly one)", "got {}".format(len(mutex_field))))

    at.print_summary()


def test_funcalls(at, path, fname, shouldcall, shouldnotcall):
    res = True
    f = parsor.get_function_by_name(path, fname)
    if f is None:
        at.record_test((False, "did not find function {} in file {}".format(fname, path), None))
        res = False
    else:
        fcalls = [c.spelling for c in parsor.get_funcalls(f)]
        for sc in shouldcall:
            at.record_test((sc in fcalls, "{} should call {}".format(fname, sc), None))
            if sc not in fcalls: res = False
        for sc in shouldnotcall:
            at.record_test((sc not in fcalls, "{} should not call {}".format(fname, sc), None))
            if sc in fcalls: res = False
    return res

@tptest('tp1-act-4-4')
def tp1_act_4_4():
    at = ActivityTestor('tp1-act-4-4')
    at.test_booted()
    at.test_console_initialized()

    ok = test_funcalls(at, "{}/kernel/sysfile.c".format(get_repopath()), "sys_create_mutex",
                  [],
                  ["begin_op",
                   "end_op",
                   "iunlockput",
                   "iunlock",
                   "ilock",
                   "dirlookup",
                   "panic"])
    if ok:
        ut = UnitTest('testator', at)
        ut.stubname = "stubs1"
        ut.add_stub_file('stubs1.c')
        ut.add_stub_file('stubs1.h')
        ut.add_stub_file('main-tp1-act-4-4.c')
        ut.add_source_fun('kernel/sysfile.c', 'sys_create_mutex')
        ut.go()

    at.print_summary()

@tptest('tp1-act-4-5')
def tp1_act_4_5():
    at = ActivityTestor('tp1-act-4-5')
    at.test_booted()
    at.test_console_initialized()
    at.test_mutest()
    at.test_mutest2()
    ok = test_funcalls(at, "{}/kernel/sysfile.c".format(get_repopath()), "sys_create_mutex",
                  [],
                  ["begin_op",
                   "end_op",
                   "iunlockput",
                   "iunlock",
                   "ilock",
                   "dirlookup",
                   "panic"])
    if ok:
        ut = UnitTest('testator', at)
        ut.stubname = "stubs1"
        ut.add_stub_file('stubs1.c')
        ut.add_stub_file('stubs1.h')
        ut.add_stub_file('main-tp1-act-4-5.c')
        ut.add_source_fun('kernel/sysfile.c', 'sys_create_mutex')
        ut.add_source_fun('kernel/sysfile.c', 'sys_acquire_mutex')
        ut.add_source_fun('kernel/sysfile.c', 'sys_release_mutex')
        ut.go()


    at.print_summary()


@tptest('tp0-init')
def tp0_init():
    at = ActivityTestor('tp0-init')
    at.test_not_booted()
    at.print_summary()

@tptest('tp1-act-5-1')
def tp1_act_5_1():
    at = ActivityTestor('tp1-act-5-1')
    at.test_booted()
    at.executator.sendline("nice-exit")
    b = at.executator.expect("PANIC")
    at.record_test((not b, "should not panic", None))
    priolock_decl = parsor.get_var_decl_by_name("{}/kernel/proc.c".format(get_repopath()), "prio_lock")
    at.record_test((priolock_decl is not None, "prio_lock presence", None))
    if priolock_decl:
        t = parsor.get_var_array_type(priolock_decl)
        if t is None:
            at.record_test((False, "prio_lock is not an array", None))
        else:
            at.record_test((t[0] == 'struct spinlock' and t[1] == 10, "prio_lock type", t))

    at.print_summary()


@tptest('tp2-act-4-1')
def tp2_act_4_1():
    at = ActivityTestor('tp2-act-4-01')
    at.test_booted()

    ut = UnitTest('testator', at)
    ut.stubname = "stubs2"
    ut.add_stub_file('stubs2.c')
    ut.add_stub_file('stubs2.h')
    ut.add_stub_file('main-tp2-act-4-1.c')
    ut.add_source_fun('kernel/proc.c', 'userinit')
    ut.add_source_fun('kernel/proc.c', 'fork')
    ut.add_source_fun('kernel/exec.c', 'exec')
    ut.add_source_fun('kernel/vm.c', 'copyout')
    ut.add_extra_file('{}/user/_ls'.format(get_repopath()), '_ls')
    ut.go()

    at.print_summary()

@tptest('tp2-act-4-3')
def tp2_act_4_3():
    at = ActivityTestor('tp2-act-4-03')
    at.test_not_booted()

    proccpath = "{}/kernel/proc.c".format(get_repopath())
    f = parsor.get_function_by_name(proccpath, "growproc")
    if f is not None:
        fcalls = [c.spelling for c in parsor.get_funcalls(f)]
        at.record_test(("uvmalloc" not in fcalls, "growproc should not call uvmalloc", None))
        at.record_test(("uvmdealloc" in fcalls, "growproc should still call uvmdealloc", None))

    ut = UnitTest('testator', at)
    ut.stubname = "stubs2"
    ut.add_stub_file('stubs2.c')
    ut.add_stub_file('stubs2.h')
    ut.add_stub_file('main-tp2-act-4-3.c')
    ut.add_source_fun('kernel/proc.c', 'growproc')
    ut.go()

    at.print_summary()

@tptest('tp2-act-4-5')
def tp2_act_4_5():
    at = ActivityTestor('tp2-act-4-05')
    at.test_not_booted()

    proccpath = "{}/kernel/vm.c".format(get_repopath())
    f = parsor.get_function_by_name(proccpath, "do_allocate")
    if f is not None:
        fcalls = [c.spelling for c in parsor.get_funcalls(f)]
        at.record_test(("walk" in fcalls, "do_allocate should call walk", None))

    ut = UnitTest('testator', at)
    ut.stubname = "stubs2"
    ut.add_stub_file('stubs2.c')
    ut.add_stub_file('stubs2.h')
    ut.add_stub_file('main-tp2-act-4-5.c')
    ut.add_source_fun('kernel/vm.c', 'do_allocate')
    ut.add_source_fun('kernel/vm.c', 'walk', onlyproto=True)
    ut.go()

    at.print_summary()

@tptest('tp2-act-4-6')
def tp2_act_4_6():
    at = ActivityTestor('tp2-act-4-06')
    at.test_booted()

    proccpath = "{}/kernel/vm.c".format(get_repopath())
    f = parsor.get_function_by_name(proccpath, "do_allocate")
    if f is not None:
        fcalls = [c.spelling for c in parsor.get_funcalls(f)]
        at.record_test(("walk" in fcalls, "do_allocate should call walk", None))
        at.record_test(("get_memory_area" in fcalls, "do_allocate should call get_memory_area", None))
        at.record_test(("mappages" in fcalls, "do_allocate should call mappages", None))
        at.record_test(("kalloc" in fcalls, "do_allocate should call kalloc", None))
        at.record_test(("kfree" in fcalls, "do_allocate should call kfree", None))


    ut = UnitTest('testator', at)
    ut.stubname = "stubs2"
    ut.add_stub_file('stubs2.c')
    ut.add_stub_file('stubs2.h')
    ut.add_stub_file('main-tp2-act-4-6.c')
    ut.add_source_fun('kernel/vm.c', 'do_allocate')
    ut.add_source_fun('kernel/vm.c', 'walk', onlyproto=True)
    ut.go()

    at.print_summary()



@tptest('tp2-act-4-11')
def tp2_act_4_11():
    at = ActivityTestor('tp2-act-4-11')
    at.test_booted()
    at.executator.sendline("rwtest")
    at.executator.expect("\$ ")
    out, err = at.executator.get_output()
    at.record_test(("HAHA!" in out, "rwtest should print HAHA", None))


    proccpath = "{}/kernel/vm.c".format(get_repopath())
    f = parsor.get_function_by_name(proccpath, "do_allocate_range")
    if f is not None:
        fcalls = [c.spelling for c in parsor.get_funcalls(f)]
        at.record_test(("walk" not in fcalls, "do_allocate_range should not call walk", None))
        at.record_test(("do_allocate" in fcalls, "do_allocate_range should call do_allocate", None))
        at.record_test(("acquire" in fcalls, "do_allocate_range should call acquire", None))
        at.record_test(("release" in fcalls, "do_allocate_range should call release", None))

    ut = UnitTest('testator', at)
    ut.stubname = "stubs2"
    ut.add_stub_file('stubs2.c')
    ut.add_stub_file('stubs2.h')
    ut.add_stub_file('main-tp2-act-4-11.c')
    ut.add_source_fun('kernel/vm.c', 'do_allocate_range')
    ut.add_source_fun('kernel/vm.c', 'walk', onlyproto=True)
    ut.go()

    at.print_summary()


@tptest('tp2-act-4-12')
def tp2_act_4_12():
    at = ActivityTestor('tp2-act-4-12')
    at.test_booted()

    proc_struct = parsor.get_struct_by_name("{}/kernel/proc.h".format(get_repopath()), "proc")
    if proc_struct is None:
        at.record_test((False, "should find 'struct proc'...", None))
    else:
        fields = parsor.get_struct_fields(proc_struct)
        field_names = [f.spelling for f in fields]
        at.record_test(("sz" not in field_names, "should no longer have a sz field", None))

    test_funcalls(at, "{}/kernel/proc.c".format(get_repopath()), "fork", ["max_addr_in_memory_areas"], [])
    test_funcalls(at, "{}/kernel/exec.c".format(get_repopath()), "exec", ["max_addr_in_memory_areas"], [])
    test_funcalls(at, "{}/kernel/proc.c".format(get_repopath()), "freeproc", ["max_addr_in_memory_areas"], [])
    test_funcalls(at, "{}/kernel/syscall.c".format(get_repopath()), "fetchaddr", ["max_addr_in_memory_areas"], [])
    test_funcalls(at, "{}/kernel/sysproc.c".format(get_repopath()), "sys_sbrk", [],["max_addr_in_memory_areas"])

    at.print_summary()

@tptest('tp2-act-5-2')
def tp2_act_5_2():
    at = ActivityTestor('tp2-act-5-02')
    at.test_booted()

    at.executator.sendline("usertests bigargtest")
    at.executator.expect("\$ ")
    out, _ = at.executator.get_output()
    at.record_test(("ALL TESTS PASSED" in out, "usertests bigargtest", out))

    at.executator.sendline("naivefib 20")
    at.executator.expect("\$ ")
    out, _ = at.executator.get_output()
    at.record_test(("unrecoverable page fault" in out, "naivefib 20 should fail", None))

    srcfile = "{}/kernel/exec.c".format(get_repopath())
    fname = "exec"
    func_cursor = parsor.get_function_by_name(srcfile, fname)
    at.record_test((func_cursor is not None,
                    "expected to find function {} in {}".format(fname, srcfile),
                    None))
    if func_cursor:
        fcalls = [c.spelling for c in parsor.get_funcalls(func_cursor)]
        fcalls_uvmalloc = [f for f in fcalls if f == "uvmalloc"]
        at.record_test((len(fcalls_uvmalloc) == 1, "exec should only call uvmalloc once (called {} times)".format(len(fcalls_uvmalloc)), None))
        at.record_test(("uvmclear" not in fcalls, "exec should not call uvmclear", None))


    ut = UnitTest('testator', at)
    ut.stubname = "stubs2"
    ut.add_stub_file('stubs2.c')
    ut.add_stub_file('stubs2.h')
    ut.add_stub_file('main-tp2-act-5-2.c')
    ut.add_source_fun('kernel/exec.c', 'exec')
    ut.add_source_fun('kernel/proc.c', 'userinit')
    ut.add_source_fun('kernel/proc.c', 'max_addr_in_memory_areas')
    ut.add_source_fun('kernel/vm.c', 'copyout')
    ut.add_extra_file('{}/user/_ls'.format(get_repopath()), '_ls')
    ut.go()

    at.print_summary()

@tptest('tp2-act-5-4')
def tp2_act_5_4():
    at = ActivityTestor('tp2-act-5-04')
    at.test_booted()

    at.executator.sendline("naivefib 20")
    at.executator.expect("\$ ")
    out, _ = at.executator.get_output()
    at.record_test(("unrecoverable page fault" not in out, "naivefib 20 should succeed", None))

    at.print_summary()



@tptest('tp2-act-6-3')
def tp2_act_6_3():
    at = ActivityTestor('tp2-act-6-03')
    at.test_booted()

    # at.executator.sendline("naivefib 20")
    # at.executator.expect("\$ ")
    # out, _ = at.executator.get_output()
    # at.record_test(("unrecoverable page fault" in out, "naivefib 20 should fail", None))

    ut = UnitTest('testator', at)
    ut.stubname = "stubs2"
    ut.add_stub_file('stubs2.c')
    ut.add_stub_file('stubs2.h')
    ut.add_stub_file('main-tp2-act-6-3.c')
    ut.add_source_fun('kernel/exec.c', 'exec')
    ut.add_source_fun('kernel/proc.c', 'userinit')
    ut.add_source_fun('kernel/proc.c', 'max_addr_in_memory_areas')
    ut.add_source_fun('kernel/vm.c', 'copyout')
    ut.add_extra_file('{}/user/_ls'.format(get_repopath()), '_ls')
    ut.go()

    at.print_summary()

@tptest('tp2-act-6-4')
def tp2_act_6_4():
    at = ActivityTestor('tp2-act-6-04')
    at.test_booted()

    srcfile = "{}/kernel/vm.c".format(get_repopath())
    fname = "do_allocate"
    func_cursor = parsor.get_function_by_name(srcfile, fname)
    at.record_test((func_cursor is not None,
                    "expected to find function {} in {}".format(fname, srcfile),
                    None))
    if func_cursor:
        args = parsor.get_fun_args(func_cursor)
        at.record_test((len(args) == 4,
                        "do_allocate should have 4 arguments (got {})".format(len(args)),
                        None))
        if len(args) >= 4:
            at.record_test((args[3][0] == "uint64",
                            "do_allocate's 4th argument should be of type uint64 (got {})".format(args[3][0]),
                            None))
            at.record_test((args[3][1] == "scause",
                            "do_allocate's 4th argument should be named scause (got {})".format(args[3][1]),
                            None))
    srcfile = "{}/kernel/vm.c".format(get_repopath())
    fname = "do_allocate_range"
    func_cursor = parsor.get_function_by_name(srcfile, fname)
    at.record_test((func_cursor is not None,
                    "expected to find function {} in {}".format(fname, srcfile),
                    None))
    if func_cursor:
        args = parsor.get_fun_args(func_cursor)
        at.record_test((len(args) == 5,
                        "do_allocate_range should have 5 arguments (got {})".format(len(args)),
                        None))
        if len(args) >= 5:
            at.record_test((args[4][0] == "uint64",
                            "do_allocate_range's 5th argument should be of type uint64 (got {})".format(args[4][0]),
                            None))
            at.record_test((args[4][1] == "scause",
                            "do_allocate_range's 5th argument should be named scause (got {})".format(args[4][1]),
                            None))

    # TODO vérifier que copyout/copyin/copyinstr envoient la bonne cause à do_allocate[_range]
    # il faut d'autres stubs, parce que copyout est déjà stubbé dans stubs2.c... (avec la mauvaise signature donc...)


    ut = UnitTest('testator', at)
    ut.stubname = "stubs2"
    ut.add_stub_file('stubs2.c')
    ut.add_stub_file('stubs2.h')
    ut.add_stub_file('main-tp2-act-6-4.c')
    ut.add_source_fun('kernel/vm.c', 'copyout')
    ut.add_source_fun('kernel/vm.c', 'copyin')
    ut.add_source_fun('kernel/vm.c', 'copyinstr')
    ut.add_source_fun('kernel/vm.c', 'do_allocate', onlyproto=True)
    ut.add_source_fun('kernel/vm.c', 'do_allocate_range', onlyproto=True)
    ut.go()

    at.print_summary()

@tptest('tp2-act-6-5')
def tp2_act_6_5():
    at = ActivityTestor('tp2-act-6-05')
    at.test_booted()

    ut = UnitTest('testator', at)
    ut.stubname = "stubs2"
    ut.add_stub_file('stubs2.c')
    ut.add_stub_file('stubs2.h')
    ut.add_stub_file('main-tp2-act-6-5.c')
    ut.add_source_fun('kernel/vm.c', 'do_allocate')
    ut.add_source_fun('kernel/vm.c', 'walk', onlyproto=True)
    ut.go()


    at.print_summary()


@tptest('tp2-act-7-1')
def tp2_act_7_1():
    at = ActivityTestor('tp2-act-7-01')
    at.test_not_booted()
    at.record_test(("instruction page fault" in at.executator.bootmsg,
                    "should obtain instruction page fault exception",
                    None))

    srcfile = "{}/kernel/exec.c".format(get_repopath())
    fname = "loadseg"
    func_cursor = parsor.get_function_by_name(srcfile, fname)
    at.record_test((func_cursor is None,
                    "expected to NOT find function {} in {}".format(fname, srcfile),
                    None))


    ut = UnitTest('testator', at)
    ut.stubname = "stubs2"
    ut.add_stub_file('stubs2.c')
    ut.add_stub_file('stubs2.h')
    ut.add_stub_file('main-tp2-act-7-1.c')
    ut.add_source_fun('kernel/exec.c', 'exec')
    ut.add_source_fun('kernel/proc.c', 'userinit')
    ut.add_source_fun('kernel/proc.c', 'max_addr_in_memory_areas')
    ut.add_source_fun('kernel/vm.c', 'copyout')
    # ut.add_source_fun('kernel/vm.c', 'do_allocate', onlyproto=True)
    # ut.add_source_fun('kernel/vm.c', 'do_allocate_range', onlyproto=True)
    ut.add_extra_file('{}/user/_ls'.format(get_repopath()), '_ls')

    ut.go()

    at.print_summary()



@tptest('tp2-act-7-2')
def tp2_act_7_2():
    at = ActivityTestor('tp2-act-7-02')
    at.test_not_booted()
    at.record_test(("illegal instruction" in at.executator.bootmsg,
                    "should obtain illegal instruction exception",
                    None))
    at.print_summary()

@tptest('tp2-act-7-3')
def tp2_act_7_3():
    at = ActivityTestor('tp2-act-7-03')
    at.test_booted()

    ut = UnitTest('testator', at)
    ut.stubname = "stubs2"
    ut.add_stub_file('stubs2.c')
    ut.add_stub_file('stubs2.h')
    ut.add_stub_file('main-tp2-act-7-3.c')
    # ut.add_source_fun('kernel/proc.c', 'userinit')
    # ut.add_source_fun('kernel/proc.c', 'max_addr_in_memory_areas')
    ut.add_source_fun('kernel/vm.c', 'do_allocate')
    ut.add_source_fun('kernel/vm.c', 'load_from_file', onlyproto=True)
    ut.add_source_fun('kernel/vm.c', 'walk', onlyproto=True)
    # ut.add_source_fun('kernel/vm.c', 'do_allocate_range', onlyproto=True)

    ut.go()

    at.print_summary()

@tptest('tp2-act-7-6')
def tp2_act_7_6():
    at = ActivityTestor('tp2-act-7-06')
    at.test_booted()

    ut = UnitTest('testator', at)
    ut.stubname = "stubs2"
    ut.add_stub_file('stubs2.c')
    ut.add_stub_file('stubs2.h')
    ut.add_source_fun('kernel/exec.c', 'exec')
    ut.add_source_fun('kernel/proc.c', 'userinit')
    ut.add_source_fun('kernel/proc.c', 'max_addr_in_memory_areas')
    ut.add_source_fun('kernel/vm.c', 'copyout')
    ut.add_extra_file('{}/user/_ls'.format(get_repopath()), '_ls')
    ut.add_stub_file('main-tp2-act-7-6.c')

    ut.go()

    at.print_summary()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--repo', type=str, help='path to repo', required=True)
    parser.add_argument('--step', type=str, help='step to check (available={})'.format(list(tests.keys())), required=True)
    parser.add_argument('--checkout', action='store_true', help='Whether to checkout branches or not')
    parser.add_argument('--ncpus', type=int, help='Number of CPUs to emulate', default=3)
    parser.add_argument('--commit', type=str, help='commit to checkout')
    parser.add_argument('--renamings', type=str, help='file to rename branches')
    parser.add_argument('--keep-tmp', action='store_true', help='whether to keep temporary files')
    parser.add_argument('--verbose', action='store_true')
    args = parser.parse_args()
    global repopath
    repopath = args.repo

    global verbose
    verbose = True if args.verbose else False

    global ncpus
    ncpus = args.ncpus

    global keep_tmps
    keep_tmps = args.keep_tmp

    if args.renamings:
        with open(args.renamings) as f:
            for line in f:
                b1, b2 = line.rstrip().split(' ')
                git_register_renaming(b1, b2)
    if args.checkout:
        global checkout
        checkout = True
    if args.commit:
        global checkout_commit
        checkout_commit = args.commit
    step = args.step
    if step in tests.keys():
        tests[step]()
    else:
        print("Unrecognized step '{}'".format(step))
        print("Please try one of the following:")
        for s in sorted(tests.keys()):
            print("  {}".format(s))

if __name__ == "__main__":
    main()
